---
title: Huddle
subtitle: Voice your Voice!
image: group-standing.jpg # https://www.pexels.com/photo/four-person-standing-on-cliff-in-front-of-sun-697243/
goals:
  - understand-more
  - pay-attention
  - get-stronger
  - celebrate
parameters:
  - parameter: social-media-type
    value: media-sharing-networks
  - parameter: timeline
    value: ongoing
  - parameter: groups-and-teams
    value: everyone-all-together
  - parameter: responsibilities
    value: choosing-group-leaders
  - parameter: openness
    value: public-showcase-events-or-celebrations
---

### Overview

Making better communities and inspiring advocacy through the coCreation of youth led media campaigns.

### Who with?

The Huddle are North Melbourne Football Club’s community arm. This project engaged local students in Melbourne.

### What was the goal?

Inspiring confidence and societal engagement through exploring the factors that contribute to cohesive communities and identifying actions that lead to change. Students work together to lead the creation of a media campaign about a topic important to their community.

### How did they achieve it?

Augmenting their existing program: Voice your Voice, an initiative designed to help young people grow their ideas and turn them into projects. Previously, only a select few ideas could be turned into short films. They embraced a ‘participatory film-making’ approach to design a project for young people to work collaboratively and ideate, capture and produce their own group films.

The term-long program (8 - 10 weeks) engaged Year 7 - 10 and VCAL students. The project consisted of three introductory workshops exploring communities, advocacy and ideation. Students then developed their skills through technical sessions focusing on storyboarding, scripting, filming and editing. The program concludes with student presentations and a celebration event.

The whole project was designed to be sustainable so that future video programs could be carried out by teachers with minimal intervention from Huddle facilitators. As such, the process was the first step in a sustainable project plan for embedding video storytelling within schools.
