---
title: Format your media
icon: /icons/format-your-media.svg
content: A bit of a simple one, but make sure you format any images and videos to suit where you’re posting. Google if you’re unsure.
modal: There are guidelines for each social media on how to format media (e.g. size, dimensions, file types) so that it is presented at its best. All of this information can be found with a quick search online, and it just helps you avoid any simple errors. There are even websites that will automatically format your media for you or help you preview what it will look like. There is nothing worse than spending ages creating the perfect piece of content only for it to be automatically cropped out of all recognition when you click post.
---
