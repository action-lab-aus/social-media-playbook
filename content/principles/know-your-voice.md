---
title: Know your voice
icon: /icons/know-your-voice.svg
content: Make sure your tone and the way you communicate on social media is consistent. This is important for building trust.
modal: What in marketing they call a ‘brand voice’. As well as helping you stand out and your work be more recognisable, staying consistent is particularly important with young people who are very careful in choosing which organisations they will engage with. This principle goes hand in hand with ‘be authentic’, as one of the cornerstones for building trust and effective communication with young people.
---
