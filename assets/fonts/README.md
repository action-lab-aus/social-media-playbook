VIC fonts belong to the Victorian Government and may only be used on authorised products/collateral.

VIC Font has been used in accordance with brand guidelines for delivering a digital product for [DFFH](https://www.vic.gov.au/department-families-fairness-and-housing).
