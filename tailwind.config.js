
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  mode: 'jit',
  purge: {
    content: [
      './dist/**/*.html',
      './pages/**/*.{js,jsx,ts,tsx,vue}',
      './components/**/*.{js,jsx,ts,tsx,vue}',
      './content/**/*.md',
    ],
  },
  darkMode: 'class',
  theme: {
    extend: {
      fontFamily: {
        sans: [
          'VIC',
          ...defaultTheme.fontFamily.sans,
        ],
      },
      animation: {
        'appear': 'appear 0.5s ease-in-out normal',
        'slideup': 'slideup 0.3s ease-in-out normal',
      },
      keyframes: {
        appear: {
          '0%': { transform: 'scale(0)', opacity: 0 },
          '20%': { transform: 'scale(0)', opacity: 0 },
          '80%': { transform: 'scale(1.2)', opacity: 1 },
          '100%': { transform: 'scale(1)', opacity: 1 },
        },
        slideup: {
          '0%': { transform: 'translateY(40px)', opacity: 0 },
          '100%': { transform: 'translateY(0%)', opacity: 1 },
        },
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
  ],
}
