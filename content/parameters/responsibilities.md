---
order: 4
title: Responsibilities
slug: responsibilities
subtitle: Giving people responsibilities can change how a group works
color: bg-pink-400
options:
  - title: Treating everyone the same
    slug: treating-everyone-the-same
    modal: In many cases, there is no need to assign responsibilities, particularly when young people are comfortable with each other and when group dynamics are already strong. If this isn’t the case however, it can be a good idea to think about things like icebreaker activities and even templating / modelling early contributions. It is always useful to establish a group identity (e.g. through team names, mascots and shared goals).
    pros:
      - Good for holding open discussions
      - Works best when the group already knows each other well
    cons:
      - Can lead to dominant personalities
      - Can also lead to less contribution from some members
  - title: Choosing group leaders
    slug: choosing-group-leaders
    modal: Giving some young people leadership roles works well when you are also using small teams. This works particularly well if that leader can help set examples for contributing to the project, encourage other young people to take part and generally act as an ambassador. Bear in mind though that not everyone likes this extra burden.
    pros:
      - A natural fit for team based work
      - Great for setting examples of how to contribute
    cons:
      - Puts an extra burden on some people
      - Some peer groups are less comfortable with leadership roles
  - title: Giving everyone a specific responsibility
    slug: giving-everyone-a-specific-responsibility
    modal: This works well when groups don’t know each other that well as it gives everyone a role and something to do. Perfect for ensuring contributions from everyone, and for breaking down complex tasks into smaller chunks (e.g. giving responsibilities for writing, audio, cinematography and directing in a film making project). Be aware though that there is more organisation work required for this, and that not all groups and tasks can be neatly divided up.
    pros:
      - Useful when breaking down complex tasks into manageable chunks
      - Good for giving people a sense of identity within a group
    cons:
      - Requires more organisational overhead
      - Doesn't fit every group size or task
---
