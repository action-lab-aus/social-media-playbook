---
title: AKO (Australian Karen Organisation - Victoria)
subtitle: Running multilingual online events with young people
image: group-on-rock.jpg # https://www.pexels.com/photo/people-friends-hiking-cliff-7625042/
imageCredit: https://www.pexels.com/photo/people-friends-hiking-cliff-7625042/
goals:
  - celebrate
  - get-stronger
parameters:
  - parameter: social-media-type
    value: social-networks
  - parameter: timeline
    value: event-series
  - parameter: groups-and-teams
    value: everyone-all-together
  - parameter: responsibilities
    value: treating-everyone-the-same
  - parameter: openness
    value: public-showcase-events-or-celebrations
---

### Overview

AKO hold regular community events to promote Karen language and culture in Victoria. Since the COVID-19 pandemic, the organisation has been grappling with shifting their events to online delivery. This project focused on upskilling young people to plan, deliver and post-produce multilingual online events that catered for both the Karen and wider Australian community.

### Who with?

The Australian Karen Organisation (AKO) Victoria is a community-based organisation that works to advocate for and represent the Karen people who resettled in Victoria, Australia. This project was with 15 AKO youth volunteers.

### What was the goal?

To build capacity in young people to help support and run multilingual online community events including planning preparation, delivery and execution and post-event engagement!

### How did they achieve it?

They held an ‘event masterclass’ workshop series involving a group of Karen youth volunteers. This took the form of 3 weekly online workshops held through video calls culminating into a final face-to-face session where the youth met event organisers and asked questions in person.

The first workshop focused on the basic skills needed to organise and run online events on Zoom, and live streaming them on social media platforms, including setting up accounts, logistics, resources, hosting and live streaming. In the second, the young people learned how to aggregate content from existing event recordings, and use a video editing tool to create short, snappy videos for the AKO social media pages. Finally, the third looked at subtitling and voice-over techniques to make the videos linguistically accessible to a wide range of audiences.
All of this built towards a final capstone community project where the young people could showcase their new skills. Group chat apps were used throughout to host group discussions and post information.

While the volunteers had prior experience with online streaming platforms as users, being on the host side of things was new to many of them. The workshops helped them gain a deeper understanding of the functions of these platforms from different perspectives. The young participants expressed their eagerness to utilise the skills they acquired to create content and raise awareness about the rich history and culture of the Karen people.
