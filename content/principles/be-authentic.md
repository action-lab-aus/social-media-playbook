---
title: Be authentic
icon: /icons/be-authentic.svg
content: There is nothing worse than jumping on trends or memes to try and seem with it. You’ll be spotted a mile off.
modal: It is far better to be authentic and honest. It might seem strange in a world of Instagram influencers and fake faces, but authenticity is an important factor in how young people relate online. Of course this is important for building trust and communication, but also because in a world of fake news and misinformation, authenticity is an increasingly valuable commodity on social media. Keep your messaging honest, ideally with a recognisable person (don’t worry about being flash), and you’ll be fine.
---
