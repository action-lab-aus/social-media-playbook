---
title: Project
---

This playbook was created as a resource for any organisation or institution who wants to use social media to more effectively engage with young people from culturally and linguistically diverse communities. It is a result of a project by Action Lab, Monash University, the Monash Migration and Inclusion Centre on _“Co-designing and scaling effective COVID-19 communication strategies for young people from culturally and linguistically diverse (CALD) communities in Victoria”_.

Although the project started with COVID-19 in focus, it quickly became clear that young people have more on their mind than the pandemic. That’s why this playbook is about all types of projects and goals that put young people at the heart of the decision making, and lets them express what matters to them through the social media they already use and are comfortable with.
