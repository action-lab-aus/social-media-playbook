---
type: chart-a-course
title: Chart a course
content: Get strategic recommendations for your organisation, include the voices of youth in your future direction.
icon: /icons/chart-a-course.svg
color: text-red-400
---
