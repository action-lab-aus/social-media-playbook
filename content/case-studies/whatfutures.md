---
title: WhatFutures
subtitle: Strategic insight with distributed youth
image: group-studying.jpg # https://www.pexels.com/photo/multiethnic-group-of-young-people-studying-with-notebook-and-laptop-6147210/
goals:
  - chart-a-course
  - celebrate
parameters:
  - parameter: social-media-type
    value: messengers
  - parameter: timeline
    value: one-off-event
  - parameter: groups-and-teams
    value: teams
  - parameter: responsibilities
    value: giving-everyone-a-specific-responsibility
  - parameter: openness
    value: public-showcase-events-or-celebrations
---

### Overview

Engaging ‘harder-to-reach’ youth in contributing to organisational strategy through a week long WhatsApp event

### Who with?

The International Federation of Red Cross and Red Crescent Societies’ globally distributed youth volunteers

### What was the goal?

To meaningfully incorporate the voices of young people into the strategic direction of the organisation. To reach engage young people typically not consulted in such processes.

### How did they achieve it?

Through the planning of a week long live game event played over WhatsApp. Young people were invited to join the game in teams, whereupon they would tackle a series of challenges designed to get them thinking about the challenges and opportunities of the future. Each young person had a specific role to play in their team. Responses to the challenges were published on a website and used within strategy documents

The game was successful in engaging nearly 4000 young people from 120 different countries in a global conversation about the future of the International Federation of Red Cross and Red Crescent Societies. WhatsApp worked well as a tool for connecting young people, and for capturing all the videos, audio and images created by the young people in response to the games’ challenges.
