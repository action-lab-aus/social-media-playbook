---
title: Know the digital environment
icon: /icons/know-the-digital-ecosystem.svg
content: Facebook for family, Snapchat for friends... Different apps are for different things, so where you choose to communicate will affect how it is perceived.
modal: That doesn’t mean just go wherever is popular, as it is important to respect boundaries. Young people need spaces that they call their own, and this is true on social media where many young people have different accounts for different uses and different sets of friends. Choosing the right social media platform that allows young people to keep their personal and professional online lives separate is important.
---
