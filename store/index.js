export const state = () => ({
  infoModal: {
    visible: false,
    icon: undefined,
    title: undefined,
    subtitle: undefined,
    content: undefined
  },
  parameterModal: {
    visible: false,
    slug: undefined,
    value: undefined
  }
})

export const getters = {
  modalsVisible(state) {
    return state.parameterModal.visible || state.infoModal.visible
  },
}

export const mutations = {
  openInfoModal(state, payload) {
    // Set modal content
    state.infoModal.icon = payload.icon ? payload.icon : undefined
    state.infoModal.title = payload.title ? payload.title : undefined
    state.infoModal.subtitle = payload.subtitle ? payload.subtitle : undefined
    state.infoModal.content = payload.content ? payload.content : undefined

    // Set modal to visible
    state.infoModal.visible = true
  },
  closeInfoModal(state) {
    // Set modal to hidden
    state.infoModal.visible = false

    // Reset modal content
    Object.keys(state.infoModal).map(function (key) {
      if (key !== 'visible') state.infoModal[key] = undefined;
    });
  },
  openParameterModal(state, payload) {
    // Set modal content
    state.parameterModal.slug = payload.slug ? payload.slug : undefined
    state.parameterModal.value = payload.value ? payload.value : undefined

    // Set modal to visible
    state.parameterModal.visible = true
  },
  closeParameterModal(state) {
    // Set modal to hidden
    state.parameterModal.visible = false

    // Reset modal content
    Object.keys(state.parameterModal).map(function (key) {
      if (key !== 'visible') state.parameterModal[key] = undefined;
    });
  }
}

export const actions = {
  openInfoModal(context, payload) {
    context.commit('openInfoModal', payload)
  },
  closeInfoModal(context) {
    context.commit('closeInfoModal')
  },
  openParameterModal(context, payload) {
    context.commit('openParameterModal', payload)
  },
  closeParameterModal(context) {
    // Prevent dismissing a nested info modal
    if (!context.state.infoModal.visible) {
      context.commit('closeParameterModal')
    }
  },
}