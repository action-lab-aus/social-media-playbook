### If you are looking to use social media with young people, then this is the playbook for you.

But wait... what even is a playbook?… We’re glad you asked! Think of this as your personal guide to the ways you can use social media in your projects with young people. This playbook will help you explore options that might work for you and give you some pointers.

### But how does it work?

The playbook is split into three sections: **goals**, **parameters** and **principles**.
**Goals** and **parameters** are designed to show you the kind of things you can do, and to give you some pros and cons for the different ways you might want to set things up. The **principles** are just that, important things to bear in mind when doing any work on social media with young people.

To back it all up we have **case studies**, so you can see how it all comes together in the real world.

If it's your first time here, we recommend you start at the top and work down, as each section builds on the previous. Or you can just jump straight in and start exploring...
