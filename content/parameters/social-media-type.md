---
order: 1
title: Social Media Type
slug: social-media-type
subtitle: Different types of social media are good for different types of projects
color: bg-green-500
options:
  - title: Social Networks
    slug: social-networks
    modal: Social networks, like Facebook and Twitter, are some of the oldest social media. Because of this, typical demographics tend to be older. However they are still great for creating shared spaces, such as private groups for specific projects, support and health groups, and for linking your project with other official organisations most of which will have social media presences on social networks.
    pros:
      - Great for creating spaces for people to come together
      - Makes connecting your project with organisations and institutions easier
    cons:
      - Not so good for reaching a young audience
      - Privacy and security are harder to control
  - title: Media Sharing Networks
    slug: media-sharing-networks
    modal: Media sharing apps like Instagram and Snapchat typically focus on users creating and sharing images and videos. They make the perfect home for strong simple messages and compelling imagery, such as for viral campaigns that highlight important issues. Bear in mind that young people’s use of media sharing networks is typically heavily curated with many young people holding different accounts for different audiences.
    pros:
      - Perfect for engagements that use lots of visuals
      - Work well for campaigns and awareness raising
    cons:
      - More difficult to organise and coordinate people
      - Less suited for text heavy or complex tasks
  - title: Messengers
    slug: messengers
    modal: Messengers, like WhatsApp and Facebook Messenger, are amongst the most popular social media on the planet and are the best at supporting direct communication between people. Typically the wealth of communication options, as well as the ability to form groups, make them perfect for creating teams and project groups. They can equally be used for keeping in touch with young people throughout a project’s duration, as well as sending information and reminders. Just bear in mind that messengers are often tied to personal data, so may not be suitable or appropriate in every context.
    pros:
      - Ideal for organising and coordinating groups of people
      - Wide variety of multimedia communication options
    cons:
      - Anonymity is a challenge as often tied to personal accounts and details (e.g. phone number)
      - Not great for public facing projects and engagements
---
