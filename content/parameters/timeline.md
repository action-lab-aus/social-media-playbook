---
order: 2
title: Timeline
slug: timeline
subtitle: Different time spans suit different communities and goals
color: bg-purple-500
options:
  - title: One-off event
    slug: one-off-event
    modal: One-off events are great for more public facing and open projects where you might not know your young people in advance. Because of this they can require lots of advance promotion and communications work, which is why they work well alongside other events. Typical uses might be setting a series of quick polls, generating responses to a ‘hashtag challenge’ or even gathering thoughts and opinions on an issue over multiple days.
    pros:
      - Ideal for large scale engagements and when used alongside real world events and festivals
      - Doesn’t require long-term commitment or motivation from young people
    cons:
      - Not as good for more complex tasks or deeper engagement
      - Success can depend a lot on pre-event promotion and comms
  - title: Event series
    slug: event-series
    modal: Short series of social media engagements work well when more depth is required, for example hosting a series of skill sessions on community podcasting, or leading a group through the stages of a design process for a new service. Generally this requires more commitment, and more organisation, but when coupled with an end celebration or showcase event, can lead to brilliant project outcomes.
    pros:
      - Great for workshops, skills sessions, and creative tasks
      - Works really well when used to build towards showcase or celebration events
    cons:
      - Generally requires a larger commitment from young people, so may put some off
      - Requires more organisation and scheduling, so not as suitable for lighter-weight engagements
  - title: Ongoing
    slug: ongoing
    modal: This is best when support and contact is needed over longer periods of time. It’s useful to think of this kind of arrangement as ‘infrastructure’ for establishing contact methods and for longer-term reporting and feedback. Typical uses could be for the establishing of support groups, peer support networks, and connections with mentors or professional development. But bear in mind, that what's relevant now might not make sense in a year or so.
    pros:
      - Good for support groups, building community and structuring ongoing relationships
      - Great for lightweight connections over time (e.g. for collecting impact evidence over a year or so)
    cons:
      - Much more likely to be impacted by the ever changing fashions and trends of social media and young people
      - Can be challenging to sustain meaningful engagement over longer periods of time
---
