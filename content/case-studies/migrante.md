---
title: Migrante Melbourne
subtitle: Mounting youth-led advocacy campaigns through social media
image: group-on-beach.jpg # https://www.pexels.com/photo/silhouette-photo-of-people-at-the-seashore-1000443/
goals:
  - understand-more
  - pay-attention
  - get-stronger
parameters:
  - parameter: social-media-type
    value: messengers
  - parameter: timeline
    value: event-series
  - parameter: groups-and-teams
    value: teams
  - parameter: responsibilities
    value: giving-everyone-a-specific-responsibility
  - parameter: openness
    value: public-showcase-events-or-celebrations
---

### Overview

To develop a social media advocacy campaign that surfaces the realities of Filipino international students during the COVID-19 lockdown in Victoria. And to use this to form a sustainable communication strategy that hinges on the creativity of the community.

### Who with?

Migrante Melbourne and its sister organisation, Anakbayan, are Filipino-led community organisations committed to the promotion of the rights and welfare of Filipino migrants based in Australia.

### What was the goal?

To advocate for the rights and welfare of young Filipinos by enabling the youth community to mount their own social media campaigns, over Facebook and Instagram, surfacing the realities of Filipino international students during the time of pandemic in Victoria. On top of the advocacy aspect, this project was also an opportunity for the organisation to strengthen the creative and leadership skills of Migrante and Anakbayan members.

### How did they achieve it?

By dividing young people into social media campaign teams, where each young person had a specific role to play (i.e. team leader, writer, art director). These teams met every week to plan and execute the different elements of the project together, including the gathering of stories, turning stories into digestible social media captions, and creating visual assets. Alongside these meetings, creative experts from Migrante Melbourne and Monash University provided skills workshops to support the teams’ content development tasks. These workshops include various topics ranging from interview techniques, to photography and graphic design. These weekly meetings and workshops were done using Facebook messenger and Zoom culminating in the soft launch of their social media campaign called Kwentong Migrante (Migrante Stories) featuring the lived experiences of Filipino international students during the pandemic lockdown in Victoria.

The project also served as an opportunity for the youth to connect to other Filipinos residing in Victoria and to learn about their experiences. One of the team members said that, prior to joining this project, he knew little about the experiences of Filipino migrants aside from his close relatives’. Interviewing Filipino international students and telling their stories through social media allowed him to understand the experiences of his fellow Filipinos, allowing him to better advocate for the rights and wellbeing of his community.
