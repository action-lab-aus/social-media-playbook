---
title: YLAB
subtitle: Developing podcasting skills through zoom workshops and peer mentoring
image: podcasting.jpg # https://www.pexels.com/photo/aluminum-audio-battery-broadcast-270288/
goals:
  - understand-more
  - get-stronger
parameters:
  - parameter: social-media-type
    value: social-networks
  - parameter: timeline
    value: event-series
  - parameter: groups-and-teams
    value: everyone-all-together
  - parameter: responsibilities
    value: choosing-group-leaders
  - parameter: openness
    value: completely-private
---

### Overview

Using cheap and freely available tech to upskill youth in collaborative podcasting.

### Who with?

YLab is a consultancy social enterprise consisting primarily of young people and their key focus is on social challenges faced by those located in Melbourne’s western suburbs.

### What was the goal?

To train youth associates in collaborative podcasting and involving community members in designing, creating and producing podcasts. All using technologies that most young people already have access to.

### How did they achieve it?

Podcasting has become an important medium in Australia for engaging young people. But many organisations have been unable to use it as a youth engagement strategy by community organisations because of the barriers associated with traditional podcasting models around skills (audio capture and editing skills) and costs (procuring equipment for audio recording and editing). We were able to design an approach to train YLab associates that relied on using social media for coordination and utilised mobile phones and low-cost tools for facilitating the entire podcasting workflow. Podcast workshops were delivered through zoom covering everything from how to choose a topic, recruiting from your community, and even writing a jingle. Each week there was a weekly challenge.
