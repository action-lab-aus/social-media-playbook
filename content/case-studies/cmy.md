---
title: CMY
subtitle: Video production with Maori-Pasifika youth
image: cameraman.jpg # https://www.pexels.com/photo/shallow-focus-photo-of-cameraman-2883160/
goals:
  - get-stronger
  - pay-attention
parameters:
  - parameter: social-media-type
    value: messengers
  - parameter: timeline
    value: event-series
  - parameter: groups-and-teams
    value: teams
  - parameter: responsibilities
    value: giving-everyone-a-specific-responsibility
  - parameter: openness
    value: public-showcase-events-or-celebrations
---

### Overview

The Le Mana leadership wants to empower the young people in the community to take on a more proactive role in awareness raising, cultural events and community consultations.

### Who with?

Center for Multicultural Youth (CMY)’s Le Mana program works with young people from Maori-Pasifika communities.

### What was the goal?

To facilitate young people in creating, editing and producing short videos that encourage participation from the wider community in community service projects such as food banks and cultural centres. A key emphasis of this project was on creating a sustainable and lowCost process for facilitating digital activities by young people by using mobile phones and social media platforms.

### How did they achieve it?

By dividing young people into video production teams, where each young person had a specific role to play. They used Facebook messenger to organise and plan 6 weeks of skills workshops with a mixture of in person, offline and online activities. Every aspect of video production was covered with the young people taking ownership over specific roles. The final videos were distributed to the wider Maori-Pasifika community and used as a basis for community discussions and follow-up activities.
