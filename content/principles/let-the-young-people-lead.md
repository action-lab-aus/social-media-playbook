---
title: Let the young people lead
icon: /icons/let-the-young-people-lead.svg
content: The more control young people have over your social media projects, the better. Chances are they are the experts, not you.
modal: This one speaks for itself, in fact you could replace the other principles with this. When you place young people at the heart of your social media projects you make them more authentic, and more powerful. This could be from letting them make decisions on how to structure and use social media in your projects, to acting as social media ambassadors, to letting them completely control your social media accounts.
---
