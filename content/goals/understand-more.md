---
type: understand-more
title: Understand more
content: Ask a community what matters to them, use social media for rapid reporting on an important issue for young people
icon: /icons/understand-more.svg
color: text-green-400
---
