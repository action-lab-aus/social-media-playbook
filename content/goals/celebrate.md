---
type: celebrate
title: Celebrate
content: Boost engagement through events. Use social media to showcase young people’s creativity and talent.
icon: /icons/celebrate.svg
color: text-blue-400
---
