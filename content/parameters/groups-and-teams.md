---
order: 3
title: Groups and Teams
slug: groups-and-teams
subtitle: The way people are organised on social media changes what you can do
image: group-size.svg
color: bg-blue-500
options:
  - title: Everyone all together
    slug: everyone-all-together
    modal: Grouping everyone together in your project can work really well when groups are smaller and/or familiar with each other. The larger social media groups get, the more likely it is that those less comfortable don't contribute. However, this isn’t always a problem, and having spaces where young people can sit back and be more passive doesn't mean that they aren’t engaging with the conversation or content.
    pros:
      - Good for groups who know each other well
      - Useful for sending out information to lots of people at once
      - Can be used as an audience for discussions and ‘live’ events
    cons:
      - Not good for encouraging contribution from people who don't normally contribute
      - Much harder for complex tasks and division of responsibility
      - Make privacy and confidentiality more challenging
  - title: Small Teams
    slug: teams
    modal: Small teams are perfect when you want to work with large groups, but still want everyone to contribute. In particular they work well for creative tasks and project based work. For example, producing a mini-film or producing a news story. Messengers make a natural fit for small teams, where the ability to group people becomes super useful.
    pros:
      - Perfect for tackling complex or creative tasks (e.g. creative writing, photography, podcasting)
      - Good for encouraging contribution from people who don’t normally contribute
      - A good fit for competitions and project based work
    cons:
      - Increases workload of organisers who need to support teams
      - Can be hard for team members to get a sense of what other teams are doing
  - title: One on One / Individual
    slug: individual
    modal: For situations when you need more privacy, or where groups just aren’t appropriate. Most social media has direct messaging features which can be used for this. A typical use is for creating mentoring links, where the content is specific to the young person, or when discussing things the young person is less comfortable discussing in group contexts. Obviously this isn’t appropriate for every context, so think carefully.
    pros:
      - Ideal for when privacy and confidentiality are important
      - Works well for mentoring or peer support
      - Good when groups don’t know each other well
    cons:
      - Increases workload of organisers
      - Not good for complex tasks/division of responsibility
      - Loses the main benefit of social media - sociability
---
