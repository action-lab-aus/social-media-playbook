process.env.VUE_APP_VERSION = process.env.npm_package_version;

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  // ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: "static",

  generate: {
    dir: "public",
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Youth Social Media Playbook",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      {
        name: "viewport",
        content:
          "width=device-width, initial-scale=1, user-scalable=yes, maximum-scale=5",
      },
      {
        hid: "description",
        name: "description",
        content:
          "Useful tips and practical advice for using social media in your projects",
      },

      // Twitter
      // Test on: https://cards-dev.twitter.com/validator
      {
        hid: "twitter:card",
        name: "twitter:card",
        content: "summary_large_image",
      },
      {
        hid: "twitter:url",
        name: "twitter:url",
        content: "",
      },
      {
        hid: "twitter:title",
        name: "twitter:title",
        content: "Youth Social Media Playbook",
      },
      {
        hid: "twitter:description",
        name: "twitter:description",
        content:
          "Useful tips and practical advice for using social media in your projects",
      },
      {
        hid: "twitter:image",
        name: "twitter:image",
        content: "/open-graph.png",
      },

      // Open Graph
      // Test on: https://developers.facebook.com/tools/debug/
      {
        hid: "og:site_name",
        property: "og:site_name",
        content: "Youth Social Media Playbook",
      },
      { hid: "og:type", property: "og:type", content: "website" },
      {
        hid: "og:url",
        property: "og:url",
        content: "",
      },
      {
        hid: "og:title",
        property: "og:title",
        content: "Youth Social Media Playbook",
      },
      {
        hid: "og:description",
        property: "og:description",
        content:
          "Useful tips and practical advice for using social media in your projects",
      },
      {
        hid: "og:image",
        property: "og:image",
        content: "/open-graph.png",
      },
      {
        hid: "og:image:secure_url",
        property: "og:image:secure_url",
        content: "/open-graph.png",
      },
      {
        hid: "og:image:alt",
        property: "og:image:alt",
        content: "Youth Social Media Playbook",
      },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.png" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~/assets/css/main.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    "@nuxtjs/tailwindcss",
    // https://www.npmjs.com/package/@nuxtjs/fontawesome
    "@nuxtjs/fontawesome",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/content
    "@nuxt/content",
    ["vue-scrollto/nuxt", { duration: 300 }],
  ],

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {
    liveEdit: true,
  },

  // Font Awesome configuration:
  fontawesome: {
    icons: {
      solid: [
        "faAngleLeft",
        "faAngleRight",
        "faAngleDown",
        "faAngleUp",
        "faTimes",
        "faHeart",
        "faCheck",
        "faQuestionCircle",
        "faCloudDownloadAlt",
        "faExternalLinkAlt",
      ],
      brands: ["faCreativeCommons", "faCreativeCommonsBy"],
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  publicRuntimeConfig: {
    version: process.env.VUE_APP_VERSION,
  },
};
