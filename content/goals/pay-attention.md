---
type: pay-attention
title: Pay attention
content: Launch awareness campaigns, use social media for advocacy and for gaining new members.
icon: /icons/pay-attention.svg
color: text-yellow-400
---
