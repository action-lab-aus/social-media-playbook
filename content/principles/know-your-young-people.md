---
title: Know your young people
icon: /icons/know-your-young-people.svg
content: Get a sense of where they are online and what they use social media for.
modal: Understanding young people’s relationship to social media is the first step to delivering effective projects on it. Learn who their favourite YouTubers are, who they watch on TikTok, which influencers they follow. The more you know, the better decisions you'll make on using social media in your projects. Plus it's always a good idea to give young people room to talk about what they enjoy and are passionate about.
---
