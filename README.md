# Youth Social Media Playbook

## About

This playbook was created as a resource for any organisation or institution who wants to use social media to more effectively engage with young people from culturally and linguistically diverse communities. It is a result of a project by Action Lab, Monash University, the Monash Migration and Inclusion Centre on “Co-designing and scaling effective COVID-19 communication strategies for young people from culturally and linguistically diverse (CALD) communities in Victoria”.

## Team

- Delvin Varghese (Action Lab)
- Joshua Seguin (Action Lab)
- Meriem Tebourbi (Action Lab)
- Patrick Olivier (Action Lab)
- Rebecca Wickes
- Rebecca Powell
- Charishma Ratnam
- Kieran Hegarty
- Tom Bartindale (Action Lab)
- Helen Skouteris
- Liam Smith
- Helen Forbes-Mewett
- Daniel Lambton-Howard
- Edward Jenkins ([Website design and development](https://edjenkins.co.uk?ref=playbook))

In addition we would like to thank all our partner organisations: [Australian Karen Organisation](https://ako.org.au/), [Center for Multicultural Youth (CMY)](https://www.cmy.net.au/), [The Huddle](https://www.nmfc.com.au/huddle), [Migrante Melbourne](https://www.facebook.com/migrantemelbourne/about/), [Anakbayan Melbourne](https://www.facebook.com/AnakbayanMelb/) and [YLab](https://www.ylab.global/).

## Table of contents

- [About](#about)
- [Team](#team)
- [Development](#development)
- [Deployment](#deployment)
  - [GitHub Pages example](#github-pages)
- [License](#license)

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Development

This is a static website built with [Nuxt.js](https://nuxtjs.org) and [Tailwind CSS](https://tailwindcss.com/).

Once you've cloned the project move into the directory and install the dependencies.

```bash
# install dependencies
$ npm install
```

You can now launch the development server

```bash
# launch dev server
$ npm run dev
```

## Deployment

This is a fully static nuxt.js website which can be hosted on any static hosting.

In order to generate the static files for hosting run the following commands:

```bash
# install dependencies
$ npm install

# generate static project
$ npm run generate
```

The output directory for the static files is '/dist'.

Dependent on the hosting provide you may need to add a CNAME file in the static directory before generating the site.

If the site is not being hosted at the root of the domain (e.g. https://example.com/playbook) you will need to update the router base in nuxt.config.js:

```bash
export default {
  router: {
    base: '/path/'
  }
}
```

### GitHub Pages

To deploy on GitHub Pages, you need to generate a static version of the website:

```bash
# generate static project
$ npm run generate
```

This will create a dist folder with everything inside ready to be deployed on GitHub Pages hosting. Branch gh-pages for project repository OR branch master for user or organization site.

If you are creating GitHub Pages for one specific repository, and you don't have any custom domain, the URL of the page will be in this format:

```bash
http://<username>.github.io/<repository-name>.
```

If you deployed dist folder without adding router base, when you visit the deployed site you will find that the site is not working due to missing assets. This is because we assume that the website root will be /, but in this case it is /<repository-name>.

To fix the issue we need to add router base configuration in nuxt.config.js:

```bash
export default {
  router: {
    base: '/<repository-name>/'
  }
}
```

**Custom domain**

In order to use a custom domain put a CNAME file in the static directory before generating the site.

[Learn more about deploying nuxt.js websites to GitHub pages](https://nuxtjs.org/docs/2.x/deployment/github-pages)

## Copyright

The content of the website is licensed and available under a [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license except for VIC fonts which is subject to [VIC Gov guidelines](https://www.vic.gov.au/brand-victoria-guidelines).

VIC fonts belong to the Victorian Government and may only be used on authorised products/collateral.

VIC fonts has been used in accordance with brand guidelines for delivering a digital product for [DFFH](https://www.vic.gov.au/department-families-fairness-and-housing).

## License

[MIT](LICENSE)
