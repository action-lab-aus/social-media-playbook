---
order: 5
title: Openness
slug: openness
subtitle: Making parts of your social media project public can make a big difference
color: bg-yellow-400
options:
  - title: Everything open to the public
    slug: everything-open-to-the-public
    modal: This works well when you don’t have a set group of young people to work with, for example when gathering wide sets of opinions and thoughts on a topic over special media, or raising awareness. It is less suited for when working more closely with a group of young people. And of course, when things are open to the public, you can get a lot of junk back, so prepare for extra moderation.
    pros:
      - Good for generating lots of ideas
      - Works for raising the profile of your project and for transparency
    cons:
      - Can discourage contributions from people less comfortable communicating publically
      - Will require extra moderation
  - title: Public showcase events or celebrations
    slug: public-showcase-events-or-celebrations
    modal: The best of both worlds, having a public event at the end of your project (e.g. to showcase what young people have produced) allows you to work closely with youth but still communicate their voice effectively to stakeholders, communities and the wider public. When coupled with short event series, these act as a perfect motivational deadline for project work.
    pros:
      - Great for focussing an activity towards an end goal
      - Perfect for publicly demonstrating results of your project
    cons:
      - Requires some foresight and planning
  - title: Completely private
    slug: completely-private
    modal: In some situations, it just might not be appropriate to make projects open to the public. But you might need to think carefully about how to capture the project (e.g. for funding and progress reports). Most social media allows you to export data, which you may need to later anonymise as appropriate.
    pros:
      - Works well for building trust within a group
      - Good for working around sensitive topics
    cons:
      - Makes it harder to include the wider community
      - Not great for demonstrating the value of your project
---
